#!/bin/bash

if [ $(whoami) != 'root' ]; then
        echo "Must be root to run $0"
        exit 1;
fi

#variables
overlayName="FPA-LCD"
uEnvPath="/boot/uEnv.txt"
capemgrPath="/etc/default/capemgr"
dtbName="am335x-boneblack-overlay.dtb"

echo "Compiling the overlay from .dts to .dtbo"
dtc -O dtb -o $overlayName-00A0.dtbo -b 0 -@ $overlayName.dts

echo "Copying .dtbo file to /lib/firmware"
cp $overlayName-00A0.dtbo /lib/firmware

echo "Updating uEnv.txt"
#create backup
cp $uEnvPath $uEnvPath.bak
#comment all dtb asignments first
sed -i s/^#*dtb=/#dtb=/g $uEnvPath
#uncomment neccessary line
sed -i s/^#*dtb=$dtbName/dtb=$dtbName/g $uEnvPath

echo "Updating capemgr"
cp $capemgrPath $capemgrPath.bak
sed -i s/^CAPE.*/CAPE=${overlayName}/g $capemgrPath
