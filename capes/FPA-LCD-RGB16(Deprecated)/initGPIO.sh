#!/bin/bash

if [ $(whoami) != 'root' ]; then
        echo "Must be root to run $0"
        exit 1;
fi

GPIO_NUMS=(32 33 34 35 36 37 38 39 44 45 46 47 61 62 63)

for i in ${GPIO_NUMS[@]}; do
	echo ${i} > /sys/class/gpio/export
done
exit 0
