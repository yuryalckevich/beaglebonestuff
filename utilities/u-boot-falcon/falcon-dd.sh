#!/bin/bash
sudo dd if=/dev/zero of=/dev/sdb bs=512 seek=1 count=98304

echo "Deploying files:"
echo ".dtb"
sudo dd if=am335x-boneblack.dtb of=/dev/sdb bs=1 seek=65536

echo "MLO"
sudo dd if=u-boot/MLO of=/dev/sdb bs=1 seek=262144

echo "u-boot"
sudo dd if=u-boot/u-boot.img of=/dev/sdb bs=1 seek=393216

echo "uImage"
sudo dd if=uImage of=/dev/sdb bs=1 seek=1179648

echo "Done..."

#-> U-BOOT commands after first start:
#setenv args_mmc 'setenv bootargs console=${console} ${optargs} root=/dev/mmcblk1p2 ro rootfstype=${mmcrootfstype}'
#setenv loadfdt 'mmc read ${fdtaddr} 80 180'
#setenv loadimage 'mmc read ${loadaddr} 900 3000'
#setenv bootcmd 'run args_mmc; run loadfdt; run loadimage; bootm ${loadaddr} - ${fdtaddr}'
#saveenv
#run args_mmc
#run loadimage
#run loadfdt
#spl export fdt ${loadaddr} - ${fdtaddr}
#mmc write ${fdtaddr} 80 180
#setenv boot_os 1
#saveenv
#reset