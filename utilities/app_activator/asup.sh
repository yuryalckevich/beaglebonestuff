#!/bin/bash

check_root(){
	if [ $(whoami) != 'root' ]; then
        echo "Must be root to run $0"
        exit 1;
	fi
}

usage(){
	printf "\nUsage:\n %s -enable app_name to enable auto startup \n %s -disable to disable auto startup\n\n" "$0" "$0"
	exit 1
}

proccess_arument(){
	case "$1" in
		"-enable")
			if [ -z "$2" ]; then
				usage
			fi
			printf "\nActivating auto startup... \n"
			enable_autostart $2
			;;
		"-disable")
			printf "\nDeactivating auto startup... \n"
			disable_autostart
			;;
		*)
			usage
	esac

}

delete_if_exist(){
	if [ -e "$1" ]; then
		rm $1
	fi
}

enable_autostart(){
	local cwd="$(pwd)"
	local scriptName="asup"
	local scriptPath="/usr/bin/${scriptName}.sh"
	local appName=$(realpath $1)
	local appPath=$(dirname $appName)

	printf "\nCreating autostart script...\n"

	delete_if_exist $scriptPath
	touch $scriptPath
	printf "#!/bin/bash\ncd $appPath\n$appName" > $scriptPath
	chmod u+x $scriptPath

	printf "\nCreating service...\n"
	cp $scriptName.service /lib/systemd/system

	printf "\nCreating symbolic link...\n"
	local symlincPath="/etc/systemd/system/$scriptName.service"
	delete_if_exist $symlincPath
	ln -s /lib/systemd/system/$scriptName.service /etc/systemd/system

	printf "\nUpdating configuration file...\n"
	systemctl daemon-reload
	systemctl enable $scriptName.service

	printf "\nRestart BBB to see results.\n"

	exit 0
}

disable_autostart(){
	local scriptName="asup"
	systemctl stop $scriptName.service
	systemctl disable $scriptName.service

	exit 0
}

check_root
proccess_arument $1 $2



