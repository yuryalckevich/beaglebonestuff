#!/bin/bash

check_root(){
	if [ $(whoami) != 'root' ]; then
        echo "Must be root to run $0"
        exit 1;
	fi
}

usage(){
	printf "\nUsage:\n$0 -prepare to init flasher once.\n\n"
	exit 1
}

delete_if_exist(){
	if [ -e "$1" ]; then
		rm $1
	fi
}

prepare(){

	local initScript="flasher_subsystem.sh"
	local copyTo="/etc/init.d/"
	local copyFrom="$(dirname $0)/$initScript"

	printf "\nCreating init script...\n\n"
	cp $copyFrom $copyTo
	local replace="sudo $(realpath $0) -flash"
	local search="#dummy"
	local fileToEdit="$copyTo/$initScript"
	sed -i "s@${search}@${replace}@g" $fileToEdit
	chmod 755 $fileToEdit

	update-rc.d $initScript defaults

	exit 0
}

flash(){
	local initScript="flasher_subsystem.sh"
	local pathToScript="/etc/init.d/$initScript"
	local dump="$(dirname $0)/bbb-eeprom.dump"
	local eeprom_path="/sys/devices/platform/ocp/44e0b000.i2c/i2c-0/0-0050/eeprom"
	dd if=$dump of=$eeprom_path

	rm $pathToScript
	update-rc.d -f $initScript remove
	reboot
	exit 0
}

check_root
case $1 in
	"-prepare")
		prepare
		;;
	"-flash")
		flash
		;;
	*)
		usage
esac