#!/bin/bash

diff(){
	awk 'BEGIN{RS=ORS=" "}
		{NR==FNR?a[$0]++:a[$0]--}
		END{for(k in a)if(a[k])print k}' <(echo -n "${!1}") <(echo -n "${!2}")
}

is_file_exists(){
	local f="$1"
	[[ -f "$f" ]] && return 0 || return 1
}

is_online(){
	wget -q --tries=5 --timeout=20 http://google.com
	if [[ $? -eq 0 ]]; then
		return 0
	else
		return 1
	fi
}

check_root(){
	if ! id | grep -q root; then
		printf "\nMust be run with sudo to proceed!\n\n"
		exit 1
	fi
	return 0
}

usage(){
	printf "\nUsage:\nsudo ./flash_script [-flash | -extract] image_name.img\n\n"
}

check_img_argument(){
	if [ -z "$1" ]; then
		printf "\nYou didn't provide image name!\n\n"
		exit 1
	fi

	if [[ $1 != *.img ]]; then
		printf "\nYou must provide only .img file!\n\n"
		exit 1
	fi
}

mount_board(){
	if ( ! is_file_exists usb_flasher)
		then
			printf "\nMissing usb_flasher application!\n"
			exit 1
		fi

		printf "\nPutting the board into flashing mode!\n\n"

		sudo ./usb_flasher
		rc=$?
		if [[ $rc != 0 ]];
		then
			echo "The board cannot be put in USB Flashing mode!"
			exit $rc
		fi

		echo -n "Waiting for the board to be mounted"
		for i in {1..12}
		do
			echo -n "."
			sleep 1
		done
		echo 
}

flash_board_with(){
	local image=$1
	check_img_argument $image

	if ( ! is_file_exists "$image")
		then
		printf "\nFile $image doesn't exist!\n\n"
		exit 1
	fi

	printf "\nFlashing board with %s!\nPlease do not insert any USB Sticks or mount external hdd during the procedure.\n" $image

	read -p "When the board is connected in USB Boot mode press [yY]." -n 1 -r
	echo
	if [[ $REPLY =~ ^[Yy]$ ]]
	then
		before=($(ls /dev | grep "sd[a-z]$"))

		mount_board

		after=($(ls /dev | grep "sd[a-z]$"))
		bbb=($(diff after[@] before[@]))
		
		if [ -z "$bbb" ];
		then
			echo "The board cannot be detected. Either it has not been"\
					" mounted or the g_mass_storage module failed loading."
			exit 1
		fi
		
		if [ ${#bbb[@]} != "1" ]
		then
			echo "You inserted an USB stick or mounted an external drive. Please "\
				"rerun the script without doing that."
			exit 1
		fi

		printf "\nThe board is mounted at /dev/%s\nFlashing eMMC now!\n" $bbb

		dd if=$image of=/dev/$bbb status=progress

		printf "\nFinished flashing onboard eMMC. Reboot the board now!\n"
	else
		exit 0
	fi
}

extract_image(){
	local image=$1
	check_img_argument $image

	printf "\nExtracting eMMC dump image from board into %s!\nPlease do not insert any USB Sticks or mount external hdd during the procedure.\n" $1

	read -p "When the board is connected in USB Boot mode press [yY]." -n 1 -r
	echo
	if [[ $REPLY =~ ^[Yy]$ ]]
	then
		before=($(ls /dev | grep "sd[a-z]$"))

		mount_board

		after=($(ls /dev | grep "sd[a-z]$"))
		bbb=($(diff after[@] before[@]))
		
		if [ -z "$bbb" ];
		then
			echo "The board cannot be detected. Either it has not been"\
					" mounted or the g_mass_storage module failed loading."
			exit 1
		fi
		
		if [ ${#bbb[@]} != "1" ]
		then
			echo "You inserted an USB stick or mounted an external drive. Please "\
				"rerun the script without doing that."
			exit 1
		fi

		printf "\nThe board is mounted at /dev/%s\nExtracting eMMC dump now!\n" $bbb

		dd if=/dev/$bbb of=$image status=progress

		printf "\nFinished exctracting onboard eMMC dump!\n"
	else
		exit 0
	fi
}

process_arguments(){
	case "$1" in
		-flash)
			flash_board_with $2
			;;
		-extract)
			extract_image $2
			;;
		*)
			usage
			exit 1
	esac
}

check_root
process_arguments "$1" "$2"
exit 0