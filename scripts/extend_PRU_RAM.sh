#!/bin/bash

if [ $(whoami) != 'root' ]; then
        echo "Must be root to run $0"
        exit 1;
fi

size=$( cat /sys/class/uio/uio0/maps/map0/size )
echo "Current PRU external memory size is $size."

if [ $size != '0x00800000' ]; then
	echo "Extending memory size to 8MB..."
	modprobe -r uio_pruss
	modprobe uio_pruss extram_pool_sz=0x00800000
	exit 1;
fi

echo "No extending is needed."


