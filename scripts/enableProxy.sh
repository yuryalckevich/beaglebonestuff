#!/bin/bash

httpProxy="http://revin:tihonovich@proxy2.bsuir.by:8080"
httpsProxy="https://revin:tihonovich@proxy2.bsuir.by:8080"
ftpProxy="ftp://revin:tihonovich@proxy2.bsuir.by:8080"

FILEenv="/etc/environment"
FILEapt="/etc/apt/apt.conf"
FILEwget="/etc/wgetrc"

flag="$1"

if [ "$flag" == "-i" ]; then 
	echo "Setting up enviroment variables..."
	sudo sh -c "echo 'http_proxy=\"'"$httpProxy"'\"' >> $FILEenv"
	sudo sh -c "echo 'https_proxy=\"'"$httpsProxy"'\"' >> $FILEenv"
	sudo sh -c "echo 'ftp_proxy=\"'"$ftpProxy"'\"' >> $FILEenv"

	echo "Setting up proxy settings for apt-get..."
	sudo sh -c "touch $FILEapt"
	sudo sh -c "echo 'Acquire::http::Proxy \"'"$httpProxy"'\";' >> $FILEapt"
	sudo sh -c "echo 'Acquire::https::Proxy \"'"$httpsProxy"'\";' >> $FILEapt"
	sudo sh -c "echo 'Acquire::ftp::Proxy \"'"$ftpProxy"'\";' >> $FILEapt"

	echo "Setting up proxy settings for wget..."
	sudo sed -i '/use_proxy =/c\use_proxy = on' $FILEwget
	sudo sed -i "s|.*https_proxy.*|https_proxy = ${httpsProxy}|" $FILEwget
	sudo sed -i "s|.*http_proxy.*|http_proxy = ${httpProxy}|" $FILEwget
	sudo sed -i "s|.*ftp_proxy.*|ftp_proxy = ${ftpProxy}|" $FILEwget

	echo "Setting up proxy for git..."
	git config --global http.proxy $httpProxy
	sudo git config --global http.proxy $httpProxy

elif [ "$flag" == "-u" ]; then
	echo "Unsetting up enviroment variables..."
	sudo sed -i '/_proxy/d' $FILEenv

	echo "Unsetting up proxy settings for apt-get..."
	sudo sed -i '/::Proxy/d' $FILEapt

	echo "Unsetting up proxy settings for wget..."
	sudo sed -i '/use_proxy =/c\use_proxy = off' $FILEwget

	echo "Unsetting up proxy for git..."	
	git config --global --unset http.proxy
	sudo git config --global --unset http.proxy

else 
	echo "Use '-i' to install, '-u' to uninstall"
fi

#don't forget about the line endings
