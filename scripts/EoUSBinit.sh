#!/bin/bash

sudo ifconfig usb0 192.168.7.2 netmask 255.255.255.252
sudo route add default gw 192.168.7.1
sudo bash -c "echo nameserver 192.168.7.1 > /etc/resolv.conf"