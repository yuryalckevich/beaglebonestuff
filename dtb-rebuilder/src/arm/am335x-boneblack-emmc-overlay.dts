/*
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
/dts-v1/;

#include "am33xx.dtsi"
#include "am33xx-es2.dtsi"
#include "am335x-bone-common.dtsi"
#include "am33xx-overlay-edma-fix.dtsi"

/ {
	model = "TI AM335x BeagleBone Black";
	compatible = "ti,am335x-bone-black", "ti,am335x-bone", "ti,am33xx";
};

&ldo3_reg {
	regulator-min-microvolt = <1800000>;
	regulator-max-microvolt = <1800000>;
	regulator-always-on;
};

&mmc1 {
	vmmc-supply = <&vmmcsd_fixed>;
};

&mmc2 {
	vmmc-supply = <&vmmcsd_fixed>;
	pinctrl-names = "default";
	pinctrl-0 = <&emmc_pins>;
	bus-width = <8>;
	status = "okay";
};

&am33xx_pinmux {
	timer4_pins: pinmux_timer4_pins {
		pinctrl-single,pins = <
				0x90  0x02 /* ( PIN_OUTPUT | MUX_MODE2 ) (R7) gpmc_advn_ale.timer4 	PA_CLK */
			>;
		};

    pru_pru_pins: pinmux_pru_pru_pins {   // The PRU pin modes
        pinctrl-single,pins = <
			0x190 0x26 /* ( PIN_INPUT | MUX_MODE6 ) (A13) mcasp0_aclkx.pr1_pru0_pru_r31[0] 		FPA_PSYNC */
			0x194 0x26 /* ( PIN_INPUT | MUX_MODE6 ) (B13) mcasp0_fsx.pr1_pru0_pru_r31[1] 		FPA_VSYNC */
			0x198 0x26 /* ( PIN_INPUT | MUX_MODE6 ) (D12) mcasp0_axr0.pr1_pru0_pru_r31[2] 		FPA_HSYNC */
			0x19c 0x26 /* ( PIN_INPUT | MUX_MODE6 ) (C12) mcasp0_ahclkr.pr1_pru0_pru_r31[3] 	FPA_DATA_0 */
			0x1a0 0x26 /* ( PIN_INPUT | MUX_MODE6 ) (B12) mcasp0_aclkr.pr1_pru0_pru_r31[4] 		FPA_DATA_1 */
			0x1a4 0x26 /* ( PIN_INPUT | MUX_MODE6 ) (C13) mcasp0_fsr.pr1_pru0_pru_r31[5] 		FPA_DATA_2 */
			0x1a8 0x26 /* ( PIN_INPUT | MUX_MODE6 ) (D13) mcasp0_axr1.pr1_pru0_pru_r31[6] 		FPA_DATA_3 */
			0x1ac 0x26 /* ( PIN_INPUT | MUX_MODE6 ) (A14) mcasp0_ahclkx.pr1_pru0_pru_r31[7] 	FPA_DATA_4 */
			0x0f0 0x26 /* ( PIN_INPUT | MUX_MODE6 ) (F17) mmc0_dat3.pr1_pru0_pru_r31[8] 		FPA_DATA_5 */
			0x0f4 0x26 /* ( PIN_INPUT | MUX_MODE6 ) (F18) mmc0_dat2.pr1_pru0_pru_r31[9] 		FPA_DATA_6 */
			0x0f8 0x26 /* ( PIN_INPUT | MUX_MODE6 ) (G15) mmc0_dat1.pr1_pru0_pru_r31[10] 		FPA_DATA_7 */
			0x0fc 0x26 /* ( PIN_INPUT | MUX_MODE6 ) (G16) mmc0_dat0.pr1_pru0_pru_r31[11] 		FPA_DATA_8 */
			0x100 0x26 /* ( PIN_INPUT | MUX_MODE6 ) (G17) mmc0_clk.pr1_pru0_pru_r31[12] 		FPA_DATA_9 */
			0x104 0x26 /* ( PIN_INPUT | MUX_MODE6 ) (G18) mmc0_cmd.pr1_pru0_pru_r31[13] 		FPA_DATA_10 */
			0x038 0x26 /* ( PIN_INPUT | MUX_MODE6 ) (V13) gpmc_ad14.pr1_pru0_pru_r31[14] 		FPA_DATA_11 */
			0x03c 0x26 /* ( PIN_INPUT | MUX_MODE6 ) (U13) gpmc_ad15.pr1_pru0_pru_r31[15] 		FPA_DATA_12 */
			0x1b4 0x25 /* ( PIN_INPUT | MUX_MODE5 ) (D14) xdma_event_intr1.pr1_pru0_pru_r31[16] FPA_DATA_13 */
        >;
    };

    bone_lcd4_cape_lcd_pins: pinmux_bone_lcd4_cape_lcd_pins {
        pinctrl-single,pins = <			   			
			0x78 0x07	/* ( PIN_OUTPUT | MUX_MODE7 ) OLED_NRST */   		
			0x24 0x08	/* ( PIN_OUTPUT | MUX_MODE0 ) lcd_data22.lcd_data22 G[0]*/
			0x30 0x08	/* ( PIN_OUTPUT | MUX_MODE0 ) lcd_data19.lcd_data19 G[1]*/
			0xb4 0x08	/* ( PIN_OUTPUT | MUX_MODE0 ) lcd_data05.lcd_data05 G[2]*/
			0xb8 0x08	/* ( PIN_OUTPUT | MUX_MODE0 ) lcd_data06.lcd_data06 G[3]*/
			0xbc 0x08	/* ( PIN_OUTPUT | MUX_MODE0 ) lcd_data07.lcd_data07 G[4]*/
			0xc0 0x08	/* ( PIN_OUTPUT | MUX_MODE0 ) lcd_data08.lcd_data08 G[5]*/
			0xc4 0x08	/* ( PIN_OUTPUT | MUX_MODE0 ) lcd_data09.lcd_data09 G[6]*/
			0xc8 0x08	/* ( PIN_OUTPUT | MUX_MODE0 ) lcd_data10.lcd_data10 G[7]*/
			0xe0 0x00   /* ( PIN_OUTPUT | MUX_MODE0 ) lcd_vsync.lcd_vsync */
			0xe4 0x00   /* ( PIN_OUTPUT | MUX_MODE0 ) lcd_hsync.lcd_hsync */
			0xe8 0x00   /* ( PIN_OUTPUT | MUX_MODE0 ) lcd_pclk.lcd_pclk */
			0xec 0x00   /* ( PIN_OUTPUT | MUX_MODE0 ) lcd_ac_bias_en.lcd_ac_bias_en */           
		>;
    };

    spi0_pins_s0: pinmux_spi0_pins_s0 {
        pinctrl-single,pins = <
			0x150 0x20	/* ( PIN_OUTPUT | MUX_MODE0 ) (A17) spi0_sclk.spi0_sclk */
			0x154 0x20	/* ( PIN_OUTPUT | MUX_MODE0 ) (B17) spi0_d0.spi0_d0 */
			0x158 0x20	/* ( PIN_OUTPUT | MUX_MODE0 ) (B16) spi0_d1.spi0_d1 */
			0x15c 0x20	/* ( PIN_OUTPUT | MUX_MODE0 ) (A16) spi0_cs0.spi0_cs0 */
        >;
    };
		 
	gpio0_pins: pinmux_gpio0_pins {
		pinctrl-single,pins = <
			//0x15c 0x37	/* ( PIN_INPUT_PULLUP | MUX_MODE7 ) (A16) spi0_cs0.gpio0[5] 			KEY_ON_INT */
			0x44 0x37	/* ( PIN_INPUT_PULLUP | MUX_MODE7 ) (V14) gpmc_a1.gpio1[17] 			ENCODER_B */
			0x5c 0x37	/* ( PIN_INPUT_PULLUP | MUX_MODE7 ) (T15) gpmc_a7.gpio1[23] 			ENCODER_KEY */
			0x60 0x37	/* ( PIN_INPUT_PULLUP | MUX_MODE7 ) (V16) gpmc_a8.gpio1[24] 			KEY_A */
			0x7c 0x37	/* ( PIN_INPUT_PULLUP | MUX_MODE7 ) (V6) gpmc_csn0.gpio1[29] 			ENCODER_A */
			0x8c 0x37	/* ( PIN_INPUT_PULLUP | MUX_MODE7 ) (V12) gpmc_clk.gpio2[1] 			KILL */
		>;
	};
};

&ocp {
	pruss: pruss@4a300000 {
		status = "okay";
		compatible = "ti,pruss-v2";
		pinctrl-names = "default";
        pinctrl-0 = <&pru_pru_pins>;
		ti,deassert-hard-reset = "pruss", "pruss";
		reg = <0x4a300000 0x080000>;
		ti,pintc-offset = <0x20000>;
		interrupt-parent = <&intc>;
		interrupts = <20 21 22 23 24 25 26 27>;

		dummy_node{

		};
	};
	gpio_helper {
        compatible = "bone-pinmux-helper";
        status = "okay";
        pinctrl-names = "default";
        pinctrl-0 = <&timer4_pins>;
    };
    spi0_pins_s0_pinmux {
        compatible = "bone-pinmux-helper";
        status = "okay";
        pinctrl-names = "default";
        pinctrl-0 = <&spi0_pins_s0>;
    };
    gpio_pinmux {
        compatible = "bone-pinmux-helper";
        status = "okay";
        pinctrl-names = "default";
        pinctrl-0 = <&gpio0_pins>;
    };
};

&lcdc {
	status = "okay";
};

&tscadc {
	status = "okay";
	adc {
		ti,adc-channels = <1>;
	};
};

/ {
	panel {
		status = "okay";
        compatible = "ti,tilcdc,panel";
        pinctrl-names = "default";
        pinctrl-0 = <&bone_lcd4_cape_lcd_pins>;
        panel-info {
            ac-bias           = <255>;
            ac-bias-intrpt    = <0>;
            dma-burst-sz      = <16>;
            bpp               = <32>;
            fdd               = <0x80>;
            sync-edge         = <0>;
            sync-ctrl         = <1>;
            raster-order      = <0>;
            fifo-th           = <0>;
        };
        display-timings {
            native-mode = <&timing0>;
            timing0: 800x600 {
                hactive         = <800>;
                vactive         = <600>;
                hback-porch     = <88>;
                hfront-porch    = <40>;
                hsync-len       = <128>;
                vback-porch     = <23>;
                vfront-porch    = <1>;
                vsync-len       = <4>;
                clock-frequency = <40000000>;
                hsync-active    = <0>;
                vsync-active    = <0>;
            };
        };
    };
};